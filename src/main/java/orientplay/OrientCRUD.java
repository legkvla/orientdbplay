package orientplay;

import com.google.common.base.Stopwatch;
import com.orientechnologies.orient.core.command.script.OCommandScript;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.intent.OIntentMassiveInsert;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchema;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * run with vm args -Xmx800m -Dstorage.diskCache.bufferSize=1500 -server -XX:+AggressiveOpts -XX:CompileThreshold=200
 * <p/>
 * User: akataev
 * Date: 01.12.14
 * Time: 23:52
 */
public class OrientCRUD {
    public static void main(String[] args) {

        ODatabaseDocumentTx db = new ODatabaseDocumentTx("plocal:/home/sunny/OrientDB/orientdb-community-1.7.10/databases/Customers");
        if (db.exists()) {
            db.open("admin", "admin");
        } else {
            db.create();
        }
        createSchema(db);

        Scanner scan = new Scanner(System.in);
        System.out.println("Ready for commands...");
        try {
            String line;
            while (!(line = scan.nextLine()).startsWith("exit")) {
                int spaceIndex = line.indexOf(' ');
                String command = line.substring(0, spaceIndex).trim();
                String arg = line.substring(spaceIndex).trim();
                Stopwatch stopwatch = Stopwatch.createStarted();

                switch (command) {
                    case "insert":
                        insert(db, Integer.parseInt(arg));
                        break;
                    case "delete":
                        delete(db);
                        break;
                    case "update":
                        update(db, arg);
                        break;
                    case "execute":
                        try {
                            execute(db, arg, true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        System.err.println("Unknown command " + line);
                }

                stopwatch.stop();
                System.out.printf("Command executed for %sms%n", stopwatch.elapsed(TimeUnit.MILLISECONDS));
            }
        } finally {
            db.close();
        }

    }

    private static void createSchema(ODatabaseDocumentTx db) {
        final OSchema schema = db.getMetadata().getSchema();
        if (!schema.existsClass("Customer")) {
            final OClass oClass = schema.createClass("Customer");

            oClass.createProperty("name", OType.STRING);
            oClass.createProperty("id", OType.INTEGER);
            schema.save();
        }
    }

    private static void insert(ODatabaseDocumentTx db, int count) {
        if (count >= 1000) {
            db.declareIntent(new OIntentMassiveInsert());
        }
        ODocument doc = new ODocument();
        for (int i = 0; i < count; i++) {
            doc.reset();
            doc.setClassName("Customer");
            doc.field("name", "Peter" + i);
            doc.field("id", i);
            db.save(doc);
        }
        db.declareIntent(null);
    }

    private static Object execute(ODatabaseDocumentTx db, String sql, boolean print) {
        Object result = db.command(new OCommandScript("sql", sql)).execute();
        if (print && result instanceof Iterable) {
            for (Object raw : (Iterable) result) {
                System.out.println(raw);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private static void update(ODatabaseDocumentTx db, String sql) {
        Iterable<ODocument> docs = (Iterable<ODocument>) execute(db, sql, false);
        Stopwatch stopwatch = Stopwatch.createStarted();

        for (ODocument doc : docs) {
            doc.field("name", "Mike" + doc.field("id"));
            doc.save();
        }

        stopwatch.stop();
        System.out.printf("Update time: %sms%n", stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    private static void delete(ODatabaseDocumentTx db) {
        db.command(new OCommandScript("sql", "delete from Customer")).execute();
    }
}

/*
Some results:

insert 10
Command executed for 9ms
insert 10000
Command executed for 1135ms
insert 100000
Command executed for 4012ms
insert 500000
Command executed for 21477ms
execute select count(*) from Customer
{count:610010}
Command executed for 17ms
update select * from Customer
// here included select time
Command executed for 126165ms

 execute select count(*) from Customer
{count:610050}
Command executed for 22ms
update select * from Customer where id < 10
Update time: 37ms
Command executed for 8207ms
update select * from Customer where id < 100
Update time: 79ms
Command executed for 6848ms
update select * from Customer where id < 10000
Update time: 1173ms
Command executed for 8051ms
update select * from Customer where id < 100000
Update time: 10370ms
Command executed for 18809ms
update select * from Customer
//memory not enough on my PC
Update time: 172999ms

75 MB - DB folder
 */
