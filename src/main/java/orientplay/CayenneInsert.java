package orientplay;

import com.google.common.base.Stopwatch;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.di.Binder;
import org.apache.cayenne.di.Module;
import org.apache.cayenne.log.JdbcEventLogger;
import org.apache.cayenne.log.NoopJdbcEventLogger;
import orientplay.cayenne.Article;

import java.util.concurrent.TimeUnit;

/**
 * User: akataev
 * Date: 30.11.14
 * Time: 23:57
 */
public class CayenneInsert {
    public static void main(String[] args) {

        Module mod = new Module() {
            @Override
            public void configure(Binder binder) {
                binder.bind(JdbcEventLogger.class).to(NoopJdbcEventLogger.class);
            }
        };
        ServerRuntime serverRuntime = new ServerRuntime("cayenne-project.xml", mod);
        ObjectContext context = serverRuntime.getContext();

        Stopwatch stopwatch = Stopwatch.createStarted();

        for (int i = 0; i < 10_000; i++) {
            Article article = context.newObject(Article.class);
            article.setTitle("I am from cayenne!");

        }
        context.commitChanges();

        stopwatch.stop();
        System.out.printf("Time elapsed: %sms%n", stopwatch.elapsed(TimeUnit.MILLISECONDS));

    }
}
