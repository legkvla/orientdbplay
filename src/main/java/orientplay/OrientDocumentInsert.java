package orientplay;

import com.google.common.base.Stopwatch;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.record.impl.ODocument;

import java.util.concurrent.TimeUnit;

/**
 * User: akataev
 * Date: 01.12.14
 * Time: 0:48
 */
public class OrientDocumentInsert {
    public static void main(String[] args) {

        ODatabaseDocumentTx tx = new ODatabaseDocumentTx("plocal:/home/sunny/OrientDB/orientdb-community-1.7.10/databases/Articles").open("admin", "admin");

        Stopwatch stopwatch = Stopwatch.createStarted();

        ODocument doc = new ODocument("Article");
        for (int i = 0; i < 10_000; i++) {
            doc.field("title", "I suspect amazing speed");
            tx.save(doc);
        }

        stopwatch.stop();

        tx.close();

        System.out.printf("Time elapsed: %sms%n", stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }
}
