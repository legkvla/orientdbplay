package orientplay;

import com.google.common.base.Stopwatch;
import com.orientechnologies.orient.core.command.script.OCommandScript;
import com.orientechnologies.orient.object.db.OObjectDatabaseTx;
import orientplay.entity.Article;

import java.util.concurrent.TimeUnit;

/**
 * User: akataev
 * Date: 30.11.14
 * Time: 23:02
 */
public class OrientSelect {
    public static void main(String[] args) {
        OObjectDatabaseTx tx = new OObjectDatabaseTx("plocal:/home/sunny/OrientDB/orientdb-community-1.7.10/databases/Articles").open("admin", "admin");
        tx.getEntityManager().registerEntityClasses("orientplay.entity");

        Stopwatch stopwatch = Stopwatch.createStarted();

        System.out.println(tx.countClass(Article.class));
        tx.command(new OCommandScript("sql", "delete from Article")).execute();
        System.out.println(tx.countClass(Article.class));

        /*for (Article article : tx.browseClass(Article.class)) {
            System.out.println(article);
        }  */

        tx.close();

        stopwatch.stop();
        System.out.printf("Time elapsed: %sms%n", stopwatch.elapsed(TimeUnit.MILLISECONDS));

    }
}
