package orientplay;

import com.google.common.base.Stopwatch;
import com.orientechnologies.orient.object.db.OObjectDatabaseTx;
import orientplay.entity.Article;
import orientplay.entity.Member;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
public class OrientLinks {
    public static void main(String[] args) {

        OObjectDatabaseTx tx = new OObjectDatabaseTx("plocal:/home/sunny/OrientDB/orientdb-community-1.7.10/databases/Articles").open("admin", "admin");
        tx.getEntityManager().registerEntityClasses("orientplay.entity");

        Stopwatch stopwatch = Stopwatch.createStarted();

        Article article = new Article();
        article.setTitle("I suspect amazing speed");
        article.setContent("Bla bla bla");
        Member member = new Member();
        member.setName("Petr");

        List<Member> members = new ArrayList<>();
        members.add(member);
        article.setAuthors(members);

        List<Article> articles = new ArrayList<>();
        articles.add(article);
        member.setArticles(articles);

        tx.save(article);
        tx.close();

        stopwatch.stop();
        System.out.printf("Time elapsed: %sms%n", stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }
}

