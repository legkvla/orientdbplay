package orientplay;

import com.google.common.base.Stopwatch;
import com.orientechnologies.orient.object.db.OObjectDatabaseTx;
import orientplay.entity.Article;

import java.util.concurrent.TimeUnit;

/**
 * User: akataev
 * Date: 30.11.14
 * Time: 23:20
 */
public class OrientInsert {
    public static void main(String[] args) {

        OObjectDatabaseTx tx = new OObjectDatabaseTx("plocal:/home/sunny/OrientDB/orientdb-community-1.7.10/databases/Articles").open("admin", "admin");
        tx.getEntityManager().registerEntityClasses("orientplay.entity");

        tx.begin();

        for (int i = 0; i < 1_000; i++) {
            Article article = new Article();
            article.setTitle("I suspect amazing speed");
            tx.save(article);
        }
        tx.commit();

        Stopwatch stopwatch = Stopwatch.createStarted();

        //tx.begin();

        for (int i = 0; i < 10_000; i++) {
            Article article = new Article();
            article.setTitle("I suspect amazing speed");
            tx.save(article);
            // if (i % 5000 == 0) {
            //   tx.commit();
            //}
        }
        //tx.commit();

        stopwatch.stop();

        tx.close();

        System.out.printf("Time elapsed: %sms%n", stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }
}
